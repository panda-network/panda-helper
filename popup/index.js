var PopupMessageType;
(function (PopupMessageType) {
    PopupMessageType["GET_PROJECTS"] = "GET_PROJECTS";
    PopupMessageType["PROJECT_CLICK"] = "PROJECT_CLICK";
})(PopupMessageType || (PopupMessageType = {}));
var AlarmType;
(function (AlarmType) {
    AlarmType["UPDATE_DATA"] = "UPDATE_DATA";
})(AlarmType || (AlarmType = {}));

const browser = chrome;
init();
async function init() {
    const projects = await sendMessage({ type: PopupMessageType.GET_PROJECTS });
    renderProjects(projects);
}
async function sendMessage(message) {
    return new Promise((resolve) => {
        browser.runtime.sendMessage(message, resolve);
    });
}
function sendProjectClick(project) {
    sendMessage({ type: PopupMessageType.PROJECT_CLICK, data: project.name });
}
function renderProjects(projects) {
    const projectFragment = document.getElementById('template-project');
    const $projects = document.getElementById('projects');
    projects.forEach((project) => {
        const $project = document.importNode(projectFragment.content, true);
        const $link = $project.querySelector('.p-link');
        $link.setAttribute('href', `https://${project.url}`);
        $link.setAttribute('title', project.descr);
        $link.addEventListener('click', async () => {
            sendProjectClick(project);
        });
        const $icon = $project.querySelector('.p-icon');
        const $name = $project.querySelector('.p-name');
        const $badge = $project.querySelector('.p-badge');
        $icon.setAttribute('src', project.images.square);
        $name.innerText = project.name;
        if (project.isNew) {
            $badge.style.display = 'inline-flex';
        }
        $projects.appendChild($project);
    });
}
