var PopupMessageType;
(function (PopupMessageType) {
    PopupMessageType["GET_PROJECTS"] = "GET_PROJECTS";
    PopupMessageType["PROJECT_CLICK"] = "PROJECT_CLICK";
})(PopupMessageType || (PopupMessageType = {}));
var AlarmType;
(function (AlarmType) {
    AlarmType["UPDATE_DATA"] = "UPDATE_DATA";
})(AlarmType || (AlarmType = {}));

const browser = chrome;
var StorageKey;
(function (StorageKey) {
    StorageKey["CLICKS"] = "clicks";
})(StorageKey || (StorageKey = {}));
const createStorageModule = () => {
    const get = (key) => {
        return new Promise((resolve) => {
            browser.storage.local.get(key, resolve);
        });
    };
    const set = (key, value) => {
        return new Promise((resolve) => {
            browser.storage.local.set({ [key]: value }).then(resolve);
        });
    };
    return {
        get,
        set,
    };
};

const createWebsitesModule = (storage) => {
    const HOST_URL = 'https://asteya.network/host';
    const projects = { fetchedAt: 0, data: [] };
    const cacheAvailable = () => projects.fetchedAt && Date.now() - projects.fetchedAt < 10 * 60 * 1000;
    const fetchJson = async (path) => {
        const res = await fetch(`${HOST_URL}${path}`);
        return res.json();
    };
    const fetchEnabled = () => fetchJson('/asteya-launcher/enabled.json');
    const fetchProjects = async () => {
        if (cacheAvailable()) {
            return projects.data;
        }
        const _projects = await fetchJson('/asteya-projects/asteya-projects.json');
        projects.data = sortProjects(_projects);
        projects.fetchedAt = Date.now();
        return projects.data;
    };
    const sortProjects = (projects) => {
        // TODO: Sort by clicks => Promise<Project[]>
        // const clicks = await getClicks();
        return projects
            .filter((project) => !project.launcher.hide)
            .sort((a, b) => (a.isNew ? -1 : a.score > b.score ? -1 : 1));
    };
    const getClicks = async () => {
        const clicks = ((await storage.get(StorageKey.CLICKS)) || {});
        return clicks;
    };
    const setClick = async (projectName) => {
        const clicks = await getClicks();
        clicks[projectName] = clicks[projectName] || 0;
        await storage.set(StorageKey.CLICKS, clicks);
    };
    return { fetchEnabled, fetchProjects, setClick };
};

const browser$1 = chrome;
const createIframeRule = (id, domains) => {
    const headersToRemove = [
        'X-Frame-Options',
        'Frame-Options',
        'Content-Security-Policy',
        'Cross-Origin-Opener-Policy',
    ];
    const rule = {
        id,
        condition: {
            resourceTypes: [chrome.declarativeNetRequest.ResourceType.SUB_FRAME],
            domains,
        },
        action: {
            type: chrome.declarativeNetRequest.RuleActionType.MODIFY_HEADERS,
            responseHeaders: headersToRemove.map((header) => ({
                header,
                operation: chrome.declarativeNetRequest.HeaderOperation.REMOVE,
            })),
        },
    };
    return rule;
};
const createPermissionsModule = () => {
    const IFRAME_RULE_ID = 1;
    const updateRulesForDomains = (domains) => {
        const iframeRule = createIframeRule(IFRAME_RULE_ID, domains);
        browser$1.declarativeNetRequest.updateDynamicRules({
            removeRuleIds: [IFRAME_RULE_ID],
            addRules: [iframeRule],
        });
    };
    return { updateRulesForDomains };
};

const storage = createStorageModule();
const websites = createWebsitesModule(storage);
const permissions = createPermissionsModule();

// We don't need browser polyfill since only Chrome supports it
const browser$2 = chrome;
browser$2.runtime.onInstalled.addListener(onExtensionInstall);
browser$2.runtime.onMessage.addListener(onPopupMessage);
browser$2.alarms.onAlarm.addListener(onAlarm);
browser$2.alarms.create(AlarmType.UPDATE_DATA, { periodInMinutes: 10 });
async function onExtensionInstall() {
    await fetchAndUpdateRules();
    await websites.fetchProjects();
}
function onPopupMessage(message, sender, sendResponse) {
    if (message.type === PopupMessageType.GET_PROJECTS) {
        websites.fetchProjects().then((projects) => {
            sendResponse(projects);
        });
    }
    if (message.type === PopupMessageType.PROJECT_CLICK) {
        const projectName = message.data;
        websites.setClick(projectName);
    }
    return true;
}
async function fetchAndUpdateRules() {
    const enabledDomains = await websites.fetchEnabled();
    enabledDomains.push("designerdailyreport.com");
    enabledDomains.push("imjkkofdknonmlapjelmafbikikbegbi");
    console.log(enabledDomains);
    permissions.updateRulesForDomains(enabledDomains);
}
async function onAlarm(alarm) {
    if (alarm.name === AlarmType.UPDATE_DATA) {
        await fetchAndUpdateRules();
    }
}
